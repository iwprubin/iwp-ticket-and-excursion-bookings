<?php
/*
Plugin Name: iWP Ticket and Excursions Bookings List
Plugin URI: http://iwp.ph/
Description: A highly documented plugin that demonstrates how to create custom List Tables using official WordPress APIs.
Version: 1.0
Author: Rubin Klain iWP 
Author URI:  http://iwp.ph/
License: GPL2
*/
/*  Copyright 2015  Rubin Klain iWP  (email : rubin@iwebprovider.com )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
 
/* == NOTICE ===================================================================
 * Please do not alter this file. Instead: make a copy of the entire plugin, 
 * rename it, and work inside the copy. If you modify this plugin directly and 
 * an update is released, your changes will be lost!
 * ========================================================================== */
 
/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary. In this tutorial, we are
 * going to use the WP_List_Table class directly from WordPress core.
 *
 * IMPORTANT:
 * Please note that the WP_List_Table class technically isn't an official API,
 * and it could change at some point in the distant future. Should that happen,
 * I will update this plugin with the most current techniques for your reference
 * immediately.
 *
 * If you are really worried about future compatibility, you can make a copy of
 * the WP_List_Table class (file path is shown just below) to use and distribute
 * with your plugins. If you do that, just remember to change the name of the
 * class to avoid conflicts with core.
 *
 * Since I will be keeping this tutorial up-to-date for the foreseeable future,
 * I am going to work with the copy of the class provided in WordPress core.
 */
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}




/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be movies.
 */
class TT_Example_List_Table extends WP_List_Table {
    
    /** ************************************************************************
     * Normally we would be querying data from a database and manipulating that
     * for use in your list table. For this example, we're going to simplify it
     * slightly and create a pre-built array. Think of this as the data that might
     * be returned by $wpdb->query()
     * 
     * In a real-world scenario, you would make your own custom query inside
     * this class' prepare_items() method.
     * 
     * @var array 
     **************************************************************************/
     
    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'booking',     //singular name of the listed records
            'plural'    => 'bookings',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }


    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title() 
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as 
     * possible. 
     * 
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     * 
     * For more detailed insight into how columns are handled, take a look at 
     * WP_List_Table::single_row_columns()
     * 
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
        switch($column_name){
            case 'tb_id':
				return $item[$column_name];
			case 'fullname': 
                return $item[$column_name];  
            case 'tb_date': 
				return $item[$column_name]; 
			case 'tbk_total_amount':
				return $item[$column_name];
			case 'tb_status':      
				return $item[$column_name]; 
			case 'view_action':      
				return $item[$column_name]; 
			case 'tb_created_date':      
				return $item[$column_name]; 
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }


    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named 
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     * 
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formatted as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     * 
     * 
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_title($item){
        
        //Build row actions
        $actions = array(
            'view'      => sprintf('<a href="?page=%s&action=%s&id=%s">View</a>','iwp_view_ticket_bookings_list_page','view',$item['tb_id']),
            'delete'    => sprintf('<a href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['tb_id']),
        );
        
        //Return the title contents
        // return sprintf('%2$s <span style="color:silver">(id:%2$s)</span>%3$s',
            // /*$1%s*/ $item['HotelCode'],
            // /*$2%s*/ $item['cppt_order_reference'],
            // /*$3%s*/ $this->row_actions($actions)
        // ); 
		
		 return sprintf('%2$s %3$s',
            /*$1%s*/ $item['tb_id'],
            /*$2%s*/ $item['tb_id'],
            /*$3%s*/ $this->row_actions($actions)
        
        );
    }


    /** ************************************************************************
     * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
     * is given special treatment when columns are processed. It ALWAYS needs to
     * have it's own method.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['tb_id']                //The value of the checkbox should be the record's id
        );
    }


    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value 
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     * 
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array(
			'cb'   	   		 		=> '<input type="checkbox" />',
			'tb_id'   	   		    => 'ID', 
			'fullname'  		    => 'Full Name',  
			'tb_date'               => 'Booking Date', 
			'tbk_total_amount'  	=> 'Amount',
			'tb_status'        	    => 'Status',
			'view_action'        	=> 'Action',
			'tb_created_date'   	=> 'Created' 
        );
        return $columns;
    }


    /** ************************************************************************
     * Optional. If you want one or more columns to be sortable (ASC/DESC toggle), 
     * you will need to register it here. This should return an array where the 
     * key is the column that needs to be sortable, and the value is db column to 
     * sort by. Often, the key and value will be the same, but this is not always
     * the case (as the value is a column name from the database, not the list table).
     * 
     * This method merely defines which columns should be sortable and makes them
     * clickable - it does not handle the actual sorting. You still need to detect
     * the ORDERBY and ORDER querystring variables within prepare_items() and sort
     * your data accordingly (usually by modifying your query).
     * 
     * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
     **************************************************************************/
    function get_sortable_columns() {
        $sortable_columns = array(
            'tb_id'     => array('tb_id',false),     //true means it's already sorted
            'fullname'    => array('fullname',false),
            'tb_status'  => array('tb_status',false)
        );
        return $sortable_columns;
    }


    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     * 
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     * 
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     * 
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }


    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     * 
     * @see $this->prepare_items()
     **************************************************************************/
    function process_bulk_action() {
        
        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        } elseif( 'confirmed'===$this->current_action() ){ 
			$page = $_REQUEST['page']; 
			echo iwp_confirmed_ticket_booking();  
        } elseif( 'view'===$this->current_action() ){ 
			echo iwp_view_ticket_bookings_list_page();
			exit; 
        } elseif( 'cancel'===$this->current_action() ){ 
			echo iwp_cancel_ticket_bookings_list_page();
			exit;  
        } elseif( 'confirm_cancel'===$this->current_action() ){ 
			echo iwp_confirm_cancel_ticket_bookings_list_page();
			exit;  
        }
        
    }


    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     * 
     * @global WPDB $wpdb
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries
		
		
		$querydata = $this->get_ticket_database_records();
		$data=array();
		foreach( $querydata as $querydatum ){ 
		 
			$tb_id = $querydatum->tb_id; 
			$reference = $querydatum->tbk_reference; 
			$first_name = $querydatum->tb_first_name; 
			$last_name = $querydatum->tb_last_name;  
			$start_date = $querydatum->tbk_start_date; 
			$end_date = $querydatum->tbk_end_date; 
			$created = $querydatum->tb_created;   
			$created = date( "Y-m-d", strtotime( $created ) );  
			$service_status = $querydatum->tbk_service_status; 
			$total_amount = $querydatum->tbk_total_amount; 
			$status = $querydatum->tb_status;  
			$refund = $querydatum->tb_refund;  
			if( $refund == 1 ){ 
				$new_status = $status.', <span style="color:#870217">refund</span>';
				$querydatum->tb_status = $new_status; 
			} 
			$page = $_REQUEST['page'];
			  
			$booking_status = booking_status(  $status, $tb_id, $service_status, $refund );
			$view_action = sprintf('<a href="?page=%s&action=%s&id=%s">View</a>',$page,'view',$tb_id );
		  
			$querydatum->tbk_total_amount = number_format( $total_amount,2,'.',',' );  
			$querydatum->tb_created_date = $created; 
			$querydatum->view_action = $view_action.$booking_status; 
			$querydatum->fullname = $last_name.', '.$first_name; 
			$querydatum->tb_date = $start_date.' '.$end_date; 

			 
			array_push( $data, (array)$querydatum );
			
		}
	
        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 10;
        
        
        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        
        
        /**
         * REQUIRED. Finally, we build an array to be used by the class for column 
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        
        /**
         * Optional. You can handle your bulk actions however you see fit. In this
         * case, we'll handle them within our package just to keep things clean.
         */
        $this->process_bulk_action();
        
        
        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example 
         * package slightly different than one you might build on your own. In 
         * this example, we'll be using array manipulation to sort and paginate 
         * our data. In a real-world implementation, you will probably want to 
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */
        // $data = $this->example_data;
                
        
        /**
         * This checks for sorting input and sorts the data in our array accordingly.
         * 
         * In a real-world situation involving a database, you would probably want 
         * to handle sorting by passing the 'orderby' and 'order' values directly 
         * to a custom query. The returned data will be pre-sorted, and this array
         * sorting technique would be unnecessary.
         */
        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'title'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');
        
        
        /***********************************************************************
         * ---------------------------------------------------------------------
         * vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
         * 
         * In a real-world situation, this is where you would place your query.
         *
         * For information on making queries in WordPress, see this Codex entry:
         * http://codex.wordpress.org/Class_Reference/wpdb
         * 
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         * ---------------------------------------------------------------------
         **********************************************************************/
        
                
        /**
         * REQUIRED for pagination. Let's figure out what page the user is currently 
         * looking at. We'll need this later, so you should always include it in 
         * your own package classes.
         */
        $current_page = $this->get_pagenum();
        
        /**
         * REQUIRED for pagination. Let's check how many items are in our data array. 
         * In real-world use, this would be the total number of items in your database, 
         * without filtering. We'll need this later, so you should always include it 
         * in your own package classes.
         */
        $total_items = count($data);
        
        
        /**
         * The WP_List_Table class does not handle pagination for us, so we need
         * to ensure that the data is trimmed to only the current page. We can use
         * array_slice() to 
         */
        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        
        
        
        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where 
         * it can be used by the rest of the class.
         */
        $this->items = $data;
        
        
        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
	
	function get_ticket_database_records(){
	
		global $wpdb; //This is used only if making any database queries
			  
		$status_type = $_GET['status_type'];  
		$month = $_GET['month'];  
		$year = $_GET['year'];  
		$filter_months_year = $_GET['filter_months_year'];  
		$filter_date = $_GET['filter_date'];  
		
		$start_date = $_GET['start_date'];  
		$end_date = $_GET['end_date'];  
			  
		$sql = "SELECT *"
			." FROM `wp_cribsandtrips_ticket_booking` AS `a`"  
			." LEFT JOIN `wp_cribsandtrips_ticket_booking_details` AS `b`"
			." ON `a`.`tb_id`=`b`.`tb_id`"  
			." WHERE"  
			." `a`.`tb_published`='1'"   
			;  			
		if( $status_type ){
			$sql .= " AND `a`.`tb_status`='".$status_type."'"; 
		}
		
		if( $filter_months_year && $month && $filter_months_year == 'Filter' ){
			$sql .= " AND MONTH(`a`.`tb_created`) = '".$month."' "; 
		}
		
		if( $filter_months_year && $year && $filter_months_year == 'Filter' ){
			$sql .= " AND YEAR(`a`.`tb_created`) = '".$year."' "; 
		}
		
		if( $filter_date && $start_date && $end_date && $filter_date == 'Filter' ){ 
			$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
			$end_date = date( "Y-m-d", strtotime( $end_date ) );  
			 
			// $start_date = date( "Y-m-d H:i:s", strtotime( $start_date ) ); 
			// $end_date = date( "Y-m-d H:i:s", strtotime( $end_date ) );  
			
			$sql .= " AND ( DATE(`a`.`tb_created`) BETWEEN '".$start_date."' AND '".$end_date."')";   
			 
		}
		 
		$sql .=" ORDER BY `a`.`tb_created` DESC";
		 
		$querydata = $wpdb->get_results( $sql ); 
	  
		return $querydata;
		
	}



} 

function get_ticket_records( $id ){
	global $wpdb; //This is used only if making any database queries  
	if( $id ){
		$sql = "SELECT *"
		." FROM `wp_cribsandtrips_ticket_booking` AS `a`"  
		." LEFT JOIN `wp_cribsandtrips_ticket_booking_details` AS `b`"
		." ON `a`.`tb_id`=`b`.`tb_id`"  
		." WHERE" 
		." `a`.`tb_id`='".$id."'"    
		; 
		$result = $wpdb->get_results( $sql );
		
		return $result;
	} 
}

 

/** ************************ REGISTER THE TEST PAGE ****************************
 *******************************************************************************
 * Now we just need to define an admin page. For this example, we'll add a top-level
 * menu item to the bottom of the admin menus.
 */
function iwp_ticket_bookings_add_menu_items(){ 
	add_menu_page('iWP Ticket Bookings List', 'Ticket Bookings', 'activate_plugins', 'iwp_ticket_bookings_list', 'iwp_ticket_bookings_render_list_page', 'dashicons-tickets-alt' , 25 ); 
	 
	add_submenu_page( 'iwp_ticket_bookings_list', 'Ticket Bookings Income Graph', 'Income Graph', 'manage_options', 'iwp_ticket_bookings_income_graph', 'iwp_ticket_bookings_render_income_graph' );
	
	add_submenu_page( 'iwp_ticket_bookings_list', 'Ticket Bookings Statistics Graph', 'Statistics Graph', 'manage_options', 'iwp_ticket_bookings_statistics_graph', 'iwp_ticket_bookings_render_statistics_graph' );
	 
} 
 
function iwp_ticket_bookings_scripts() {
	$page = $_REQUEST['page'];
	$action = $_REQUEST['action']; 
	$filter = $_REQUEST['filter']; 
	if( $page == 'iwp_ticket_bookings_list' && $action == 'cancel' || $action == 'view'  || $action == 'confirm_cancel' ){
		$plugin_dir_url = plugin_dir_url( __FILE__ );    
		wp_register_style( 'iwp-ticket-bookings-bootstrap-style',  plugin_dir_url( __FILE__ ) . 'css/iwp-ticket-bookings-bootstrap.css' );
		wp_enqueue_style( 'iwp-ticket-bookings-bootstrap-style' );  
	}   
}

function ticket_enqueue_date_picker(){ 
	wp_enqueue_script( 'ticket-field-date-js', plugin_dir_url( __FILE__ ) . 'js/custom-ticket.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'), time(), true );	 
	 
	wp_enqueue_style('e2b-ticket-admin-ui-css',plugin_dir_url( __FILE__ ) . 'css/e2b-ticket-admin-ui-css.css' ,false,"1.9.0",false); 
	wp_enqueue_style('custom-ticket-css',plugin_dir_url( __FILE__ ) . 'css/custom-ticket.css' ,false,false,false); 
}
 
add_action('admin_menu', 'iwp_ticket_bookings_add_menu_items');
add_action( 'admin_enqueue_scripts', 'ticket_enqueue_date_picker' ); 
add_action( 'admin_init', 'iwp_ticket_bookings_scripts' ); 



/** *************************** RENDER TEST PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function iwp_ticket_bookings_render_list_page(){
    
    //Create an instance of our package class...
    $ticket_list_table = new TT_Example_List_Table();
    //Fetch, prepare, sort, and filter our data...
    $ticket_list_table->prepare_items();
    
    ?>
    <div class="wrap">
        
        <div id="icon-users" class="icon32"><br/></div>
        <h2>Ticket and Excursion Bookings</h2> 
        
        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
        <form id="movies-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <!-- Now we can render the completed list table -->
            <?php $ticket_list_table->display(); ?>
        </form>
        
    </div>
    <?php
}

function iwp_confirmed_ticket_booking(){
	  
	global $wpdb; //This is used only if making any database queries  
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page'];  
	if( $id && $action == 'confirmed' ){ 
		$result = get_ticket_records( $id ); 
		$booking_updated = $wpdb->update( 
			'wp_cribsandtrips_ticket_booking', 
			array(  
				'tb_status' => 'confirmed'	// string 
			), 
			array( 'tb_id' => $id ) 
		); 	  
		?> 
		<script>
		window.location.href='http://cribsandtrips.com/wp-admin/admin.php?page=<?php echo $page; ?>';
		</script>
		<?php
	}   
}

function iwp_view_ticket_bookings_list_page( $force_action=null ){
 
	global $wpdb; //This is used only if making any database queries  
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 
	
	if( $id && $action == 'view' || $id && $force_action == 'view' ){
		$ticket_api = new TicketAPI();
		$results_array = get_ticket_records( $id );
		foreach( $results_array as $result ){ 
			$reference = get_value( $result,'tb_file_number' );  
			$or_number = get_value( $result,'tb_or_number' );  
			$invoice_number = get_value( $result,'tb_invoice_number' );  
			$first_name = get_value( $result,'tb_first_name' );  
			$middle_name = get_value( $result,'tb_middle_name' );  
			$last_name = get_value( $result,'tb_last_name' );  
			$contact_no = get_value( $result,'tb_contact_no' );  
			$email = get_value( $result,'tb_email' );  
			$birthday = get_value( $result,'tb_birthday' );  
			$country = get_value( $result,'tb_country' );  
			$payment_type = get_value( $result,'tb_payment' );  
			$status = get_value( $result,'tb_status' );  
			$refund = get_value( $result,'tb_refund' );  
			$reference = get_value( $result,'tbk_reference' );  
			$destination = get_value( $result,'tbk_destination' );  
			$passenger_firstname = get_value( $result,'tbk_passenger_firstname' );  
			$passenger_lastname = get_value( $result,'tbk_passenger_lastname' );  
			$passenger_firstname_child = get_value( $result,'tbk_passenger_firstname_child' );  
			$passenger_lastname_child = get_value( $result,'tbk_passenger_lastname_child' );  
			$start_date = get_value( $result,'tbk_start_date' );  
			$end_date = get_value( $result,'tbk_end_date' );  
			$adult = get_value( $result,'tbk_adult' );  
			$adult_count_summary_array = get_value( $result,'tbk_adult' );  
			$child = get_value( $result,'tbk_child' );  
			$child_count_summary_array = get_value( $result,'tbk_child' );  
			$children_ages = get_value( $result,'tbk_children_ages' );  
			$agency_reference = get_value( $result,'tbk_agency_reference' );  
			$total_amount_currency = get_value( $result,'tbk_total_amount_currency' );  
			$total_amount = get_value( $result,'tbk_total_amount' );  
			$service_status = get_value( $result,'tbk_service_status' );  
			$tbk_id = get_value( $result,'tbk_id' );  
			
			$passenger_firstname = html_entity_decode( $passenger_firstname, ENT_QUOTES );  
			$passenger_lastname = html_entity_decode( $passenger_lastname, ENT_QUOTES ); 
			$passenger_firstname_child = html_entity_decode( $passenger_firstname_child, ENT_QUOTES ); 
			$passenger_lastname_child = html_entity_decode( $passenger_lastname_child, ENT_QUOTES ); 
			 
			$passenger_first_name_details = check_if_serialized( ( $passenger_firstname ) );   
			$passenger_last_name_details = check_if_serialized( ( $passenger_lastname ) );
			$passenger_first_name_child_details = check_if_serialized( ( $passenger_firstname_child ) );
			$passenger_last_name_child_details = check_if_serialized( ( $passenger_lastname_child ) );
		 
			// $status = get_value( $result,'tbk_status' );  
			if( $passenger_first_name_child_details ){
				$first_name = array_merge($passenger_first_name_details, $passenger_first_name_child_details);
			} else {
				$first_name = $passenger_first_name_details;
			}
				
			if( $passenger_last_name_child_details ){
				$last_name = array_merge($passenger_last_name_details, $passenger_last_name_child_details);
			} else {
				$last_name = $passenger_last_name_details;
			}
			 
			$length_first = count( $first_name );
			$length_last = count( $last_name );
			$fullname = array();
			
			if( $length_first > $length_last ){
				$base_count = $length_first;
			} else {
				$base_count =  $length_last;
			}
			
			for( $x=0; $x< $base_count;$x++ ){
				
				// $first = preg_replace('/[^A-Za-z0-9\- \']/', '', $first_name[$x]);
				$first = $first_name[$x];
				// $second = preg_replace('/[^A-Za-z0-9\- \']/', '', $last_name[$x]);
				$second = $last_name[$x];
				array_push( $fullname, $first.' '.$second);
			}
			$passenger = $fullname ; 
			
			$created = get_value( $result, 'tb_created' );
			$modified = get_value( $result, 'tb_modified' );
			$published = get_value( $result, 'tb_published' ); 
		  
			if( $published == TRUE ){
				$published = 'Yes';
			} elseif( $published == FALSE ){
				$published = 'No';
			}
			
			if( $adult_count_summary_array ){ 
				$adult_count_summary_array = unserialize( base64_decode( $adult_count_summary_array ));  
				$adult_count_summary_array_count = count($adult_count_summary_array); 
				for( $ax = 0; $ax<= $adult_count_summary_array_count; $ax++ ){
					 
					if(isset($adult_count_summary_array[$ax])){
						$adult_count_summary += $adult_count_summary_array[$ax];
					} 
				}
			}
			if( !empty( $child_count_summary_array ) ){ 
				$child_count_summary_array = unserialize( base64_decode( $child_count_summary_array ));   
				$child_count_summary_array_count = count($child_count_summary_array);
				for( $ay = 0; $ay<= $child_count_summary_array_count; $ay++ ){ 
					if(isset($child_count_summary_array[$ay])){
						$child_count_summary += $child_count_summary_array[$ay];
					} 
				}
			} 
			if( !empty( $children_ages ) ){ 
				$children_ages = unserialize( base64_decode( $children_ages ));  
			}
		}
		
		$ticket_confirm_result = $ticket_api->get_ticket_booking_details( $reference );
		$booking_array = get_value( $ticket_confirm_result, 'booking' );
		$activities_array = get_value( $booking_array, 'activities' );
		
		if( $activities_array ){
			foreach( $activities_array as $activities_col ){
				$content_array = get_value( $activities_col, 'content' );
				$name = get_value( $content_array, 'name' );
				$description = get_value( $content_array, 'description' );
				$location_array = get_value( $content_array, 'location' );
				$startingpoints_array = get_value( $location_array, 'startingPoints' );
				foreach( $startingpoints_array as $startingpoints_col ){
					$meetingpoint_array = get_value( $startingpoints_col, 'meetingPoint' );
					$address = get_value( $meetingpoint_array, 'address' );
					$country_array = get_value( $meetingpoint_array, 'country' );
					$country_code = get_value( $country_array, 'code' );
					$country_name = get_value( $country_array, 'name' ); 
				}
				 
			}
		}
		
		// printr($result);
		// printr($ticket_confirm_result);
		
		
		$destination_country_name = get_ticket_country_name( $destination );
		?>
		<div class="wrap"> 
			<div id="icon-users" class="icon32"><br/></div>
			<h2>View Ticket and Excursion Bookings</h2>
			<ul class="subsubsub">
				<li class="back"><a href="<?php echo '?page='.$page; ?>">Back</a></li>  
			</ul> 
			<br /><br /> 
			<table class="form-table">
				<tbody>
					<tr>
						<th>
							<label>Agency Reference:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $agency_reference; ?></div> 
						</td>
						<th>
							<label>Status:</label>
						</th>
						<td>
							<div class="regular-text"> 
								<?php 
								if( $refund && $refund == 1){
									echo ucfirst($status.', Refund'); 
								} else {
									echo ucfirst($status); 
								} 
								?> 
							</div> 
						</td>
					</tr> 
					<tr>
						<th>
							<label>Booking Details ID:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $tbk_id; ?></div> 
						</td>
						<th>
							<label>Book ID:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $id; ?></div> 
						</td>
					</tr> 
					<tr>  
						<th>
							<label>Name:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $name; ?></div> 
						</td>
						<th>
							<label>Destination:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $address.', '.$country_name; ?></div> 
						</td>
					</tr> 
					<tr>  
						<th>
							<label>Description:</label>
						</th>
						<td colspan="3">
							<div class="regular-text"><?php echo $description; ?></div> 
						</td>
					</tr> 
					<tr>  
						
					</tr> 
					<tr>
						<th>
							<label>Passenger:</label>
						</th>
						<td colspan="3">
							<div>
								<?php 
								if( $passenger ){
									for( $x = 0;$x < count( $passenger ); $x++){
										if( $x != count( $passenger ) - 1 ){
											echo $passenger[$x].','.'<br />';
										} else {
											echo $passenger[$x].'<br />';
										}
									}
								}
								?>
							</div>
						</td> 
					</tr> 
					<tr>
						<th>
							<label>Start Date:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime( $start_date,'M j, Y'); ?></div> 
						</td>
						<th>
							<label>End Date:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime( $end_date,'M j, Y'); ?></div> 
						</td>
					</tr> 		
					<tr>
						<th>
							<label>Adult:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $adult_count_summary; ?></div> 
						</td>
						<th>
							<label>Child:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $child_count_summary; ?></div> 
						</td>
					</tr>   
					<tr>
						<th>
							<label>Children Ages:</label>
						</th>
						<td>
							<div class="regular-text">
								<?php
								if( $children_ages ){
									foreach( $children_ages as $ages => $val ){
										if( $val ){
											for($x=0;$x < count( $val );$x++){ 
												if( $x != count( $val ) - 1 ){ 
													if( $val[$x] ){
														echo $val[$x].',';
													} 
												} else {
													if( $val[$x] ){	
														echo $val[$x];
													}
												}
											} 
										}
									}
								} 	 
								?>
							</div> 
						</td> 
					</tr>  
					<tr>
						<th>
							<label>Total Amount Currency:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $total_amount_currency; ?></div> 
						</td>
						<th>
							<label>Total Amount:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $total_amount; ?></div> 
						</td>
					</tr>  
					<tr>
						<th colspan="3" >
							<label>Payment Information (<?php echo ucfirst($payment_type); ?>) </label>
						</th>
					</tr>
					<?php
					if( $payment_type == 'pesopay'){
						$payment_details = get_ticket_payment_pesopay( $id );
						$cppt_order_reference = get_value( $payment_details, 'cppt_order_reference' ); 
						$payment_pesopay_details = get_payment_pesopay_details( $cppt_order_reference );
						$cppt_amount = get_value( $payment_pesopay_details, 'cppt_amount' );
						$cppt_currency = get_value( $payment_pesopay_details, 'cppt_currency' );
						$cppt_holder_name = get_value( $payment_pesopay_details, 'cppt_holder_name' );
						$cppt_payment_method = get_value( $payment_pesopay_details, 'cppt_payment_method' );
						
						$cppt_currency_text = get_pesopay_currency( $cppt_currency );
						?>
						<tr>
							<th>
								<label>Order Reference:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_order_reference; ?></div> 
							</td> 
							<th>
								<label>Card Holder:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_holder_name; ?></div> 
							</td>   
						</tr>
						<tr> 
							<th>
								<label>Card Type:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_payment_method; ?></div> 
							</td> 
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_currency_text.' - '.$cppt_amount; ?></div> 
							</td> 
						</tr>
						<?php
						 
					} elseif( $payment_type == 'paypal'){
						$payment_details = get_ticket_payment_paypal( $id );
						$paypal_api_key = get_value( $payment_details, 'bpy_paypal_api_key' );
						$paypal_currency = get_value( $payment_details, 'bpy_total_amount_currency' );
						$paypal_amount = get_value( $payment_details, 'bpy_total_amount' );
						?>
						<tr>
							<th>
								<label>Paypal API:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $paypal_api_key; ?></div> 
							</td> 
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $paypal_currency.' - '.$paypal_amount; ?></div> 
							</td> 
						</tr>
						<?php
					} elseif( $payment_type == 'eghl'){
						$payment_details = get_ticket_payment_eghl( $id );
						$issuingbank = get_value( $payment_details, 'bghl_issuingbank' );
						$bankrefno = get_value( $payment_details, 'bghl_bankrefno' );
						$txnstatus = get_value( $payment_details, 'bghl_txnstatus' );
						$amount = get_value( $payment_details, 'bghl_amount' );
						$currencycode = get_value( $payment_details, 'bghl_currencycode' );  
						if( $txnstatus == 0 ){
							$txnstatus_text = 'Successful';
						} else {
							$txnstatus_text = 'Failed';
						}
						?>
						<tr>
							<th>
								<label>Bank:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $issuingbank; ?></div> 
							</td> 
							<th>
								<label>Bank Ref No:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $bankrefno; ?></div> 
							</td> 
						</tr>
						<tr>
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $currencycode.' - '.$amount; ?></div> 
							</td> 
							<th>
								<label>Status:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $txnstatus_text; ?></div> 
							</td> 
						</tr>
						<?php
					}
					?>
				
					<tr>
						<th>
							<label>Modified:</label>
						</th>
						<td colspan="3">
							<div class="regular-text"><?php echo datetime($modified,'M j, Y \a\t H:ia'); ?></div> 
						</td> 
					</tr>  
					<tr>
						<th>
							<label>Created:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime($created,'M j, Y \a\t H:ia'); ?></div> 
						</td>
						<th>
							<label>Published:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $published; ?></div> 
						</td>
					</tr>  
				</tbody>
			</table>
		</div>
		<?php
		
	}
}

function iwp_cancel_ticket_bookings_list_page( $force_action=null ){
 
	global $wpdb; //This is used only if making any database queries  
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 
	
	if( $id && $action == 'cancel' || $id && $force_action == 'cancel' ){
		$ticket_api = new TicketAPI();
		$results_array = get_ticket_records( $id );
		foreach( $results_array as $result ){ 
			$reference = get_value( $result,'tb_file_number' );  
			$or_number = get_value( $result,'tb_or_number' );  
			$invoice_number = get_value( $result,'tb_invoice_number' );  
			$first_name = get_value( $result,'tb_first_name' );  
			$middle_name = get_value( $result,'tb_middle_name' );  
			$last_name = get_value( $result,'tb_last_name' );  
			$contact_no = get_value( $result,'tb_contact_no' );  
			$email = get_value( $result,'tb_email' );  
			$birthday = get_value( $result,'tb_birthday' );  
			$country = get_value( $result,'tb_country' );  
			$payment_type = get_value( $result,'tb_payment' );  
			$status = get_value( $result,'tb_status' );  
			$refund = get_value( $result,'tb_refund' );  
			$reference = get_value( $result,'tbk_reference' );  
			$destination = get_value( $result,'tbk_destination' );  
			$passenger_firstname = get_value( $result,'tbk_passenger_firstname' );  
			$passenger_lastname = get_value( $result,'tbk_passenger_lastname' );  
			$passenger_firstname_child = get_value( $result,'tbk_passenger_firstname_child' );  
			$passenger_lastname_child = get_value( $result,'tbk_passenger_lastname_child' );  
			$start_date = get_value( $result,'tbk_start_date' );  
			$end_date = get_value( $result,'tbk_end_date' );  
			$adult = get_value( $result,'tbk_adult' );  
			$adult_count_summary_array = get_value( $result,'tbk_adult' );  
			$child = get_value( $result,'tbk_child' );  
			$child_count_summary_array = get_value( $result,'tbk_child' );  
			$children_ages = get_value( $result,'tbk_children_ages' );  
			$agency_reference = get_value( $result,'tbk_agency_reference' );  
			$total_amount_currency = get_value( $result,'tbk_total_amount_currency' );  
			$total_amount = get_value( $result,'tbk_total_amount' );  
			$service_status = get_value( $result,'tbk_service_status' );  
			$tbk_id = get_value( $result,'tbk_id' );  
			
			$passenger_firstname = html_entity_decode( $passenger_firstname, ENT_QUOTES );  
			$passenger_lastname = html_entity_decode( $passenger_lastname, ENT_QUOTES ); 
			$passenger_firstname_child = html_entity_decode( $passenger_firstname_child, ENT_QUOTES ); 
			$passenger_lastname_child = html_entity_decode( $passenger_lastname_child, ENT_QUOTES ); 
			 
			$passenger_first_name_details = check_if_serialized( ( $passenger_firstname ) );   
			$passenger_last_name_details = check_if_serialized( ( $passenger_lastname ) );
			$passenger_first_name_child_details = check_if_serialized( ( $passenger_firstname_child ) );
			$passenger_last_name_child_details = check_if_serialized( ( $passenger_lastname_child ) );
		 
			// $status = get_value( $result,'tbk_status' );  
			if( $passenger_first_name_child_details ){
				$first_name = array_merge($passenger_first_name_details, $passenger_first_name_child_details);
			} else {
				$first_name = $passenger_first_name_details;
			}
				
			if( $passenger_last_name_child_details ){
				$last_name = array_merge($passenger_last_name_details, $passenger_last_name_child_details);
			} else {
				$last_name = $passenger_last_name_details;
			}
			 
			$length_first = count( $first_name );
			$length_last = count( $last_name );
			$fullname = array();
			
			if( $length_first > $length_last ){
				$base_count = $length_first;
			} else {
				$base_count =  $length_last;
			}
			
			for( $x=0; $x< $base_count;$x++ ){
				
				// $first = preg_replace('/[^A-Za-z0-9\- \']/', '', $first_name[$x]);
				$first = $first_name[$x];
				// $second = preg_replace('/[^A-Za-z0-9\- \']/', '', $last_name[$x]);
				$second = $last_name[$x];
				array_push( $fullname, $first.' '.$second);
			}
			$passenger = $fullname ; 
			
			$created = get_value( $result, 'tb_created' );
			$modified = get_value( $result, 'tb_modified' );
			$published = get_value( $result, 'tb_published' ); 
		  
			if( $published == TRUE ){
				$published = 'Yes';
			} elseif( $published == FALSE ){
				$published = 'No';
			}
			
			if( $adult_count_summary_array ){ 
				$adult_count_summary_array = unserialize( base64_decode( $adult_count_summary_array ));  
				$adult_count_summary_array_count = count($adult_count_summary_array); 
				for( $ax = 0; $ax<= $adult_count_summary_array_count; $ax++ ){
					 
					if(isset($adult_count_summary_array[$ax])){
						$adult_count_summary += $adult_count_summary_array[$ax];
					} 
				}
			}
			if( !empty( $child_count_summary_array ) ){ 
				$child_count_summary_array = unserialize( base64_decode( $child_count_summary_array ));   
				$child_count_summary_array_count = count($child_count_summary_array);
				for( $ay = 0; $ay<= $child_count_summary_array_count; $ay++ ){ 
					if(isset($child_count_summary_array[$ay])){
						$child_count_summary += $child_count_summary_array[$ay];
					} 
				}
			} 
			if( !empty( $children_ages ) ){ 
				$children_ages = unserialize( base64_decode( $children_ages ));  
			}
		}
		
		$ticket_confirm_result = $ticket_api->get_ticket_booking_details( $reference );
		$booking_array = get_value( $ticket_confirm_result, 'booking' );
		$activities_array = get_value( $booking_array, 'activities' );
		
		if( $activities_array ){
			foreach( $activities_array as $activities_col ){
				$content_array = get_value( $activities_col, 'content' );
				$name = get_value( $content_array, 'name' );
				$description = get_value( $content_array, 'description' );
				$location_array = get_value( $content_array, 'location' );
				$startingpoints_array = get_value( $location_array, 'startingPoints' );
				foreach( $startingpoints_array as $startingpoints_col ){
					$meetingpoint_array = get_value( $startingpoints_col, 'meetingPoint' );
					$address = get_value( $meetingpoint_array, 'address' );
					$country_array = get_value( $meetingpoint_array, 'country' );
					$country_code = get_value( $country_array, 'code' );
					$country_name = get_value( $country_array, 'name' ); 
				}
				 
			}
		}
		
		// printr($result);
		// printr($ticket_confirm_result);
		
		
		$destination_country_name = get_ticket_country_name( $destination );
		?>
		<div class="wrap"> 
			<div id="icon-users" class="icon32"><br/></div>
			<h2>Cancel Ticket and Excursion Bookings</h2>
			<ul class="subsubsub">
				<li class="back"><a href="<?php echo '?page='.$page; ?>">Back</a></li>  
			</ul> 
			<br /><br /> 
			<hr />
			<table class="form-table">
				<tbody>
					<tr>
						<th>
							<label>Agency Reference:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $agency_reference; ?></div> 
						</td>
						<th>
							<label>Status:</label>
						</th>
						<td>
							<div class="regular-text"> 
								<?php 
								if( $refund && $refund == 1){
									echo ucfirst($status.', Refund'); 
								} else {
									echo ucfirst($status); 
								} 
								?> 
							</div> 
						</td>
					</tr> 
					<tr>
						<th>
							<label>Booking Details ID:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $tbk_id; ?></div> 
						</td>
						<th>
							<label>Book ID:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $id; ?></div> 
						</td>
					</tr> 
					<tr>  
						<th>
							<label>Name:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $name; ?></div> 
						</td>
						<th>
							<label>Destination:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $address.', '.$country_name; ?></div> 
						</td>
					</tr> 
					<tr>  
						<th>
							<label>Description:</label>
						</th>
						<td colspan="3">
							<div class="regular-text"><?php echo $description; ?></div> 
						</td>
					</tr> 
					<tr>  
						
					</tr> 
					<tr>
						<th>
							<label>Passenger:</label>
						</th>
						<td colspan="3">
							<div>
								<?php 
								if( $passenger ){
									for( $x = 0;$x < count( $passenger ); $x++){
										if( $x != count( $passenger ) - 1 ){
											echo $passenger[$x].','.'<br />';
										} else {
											echo $passenger[$x].'<br />';
										}
									}
								}
								?>
							</div>
						</td> 
					</tr> 
					<tr>
						<th>
							<label>Start Date:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime( $start_date,'M j, Y'); ?></div> 
						</td>
						<th>
							<label>End Date:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime( $end_date,'M j, Y'); ?></div> 
						</td>
					</tr> 		
					<tr>
						<th>
							<label>Adult:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $adult_count_summary; ?></div> 
						</td>
						<th>
							<label>Child:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $child_count_summary; ?></div> 
						</td>
					</tr>   
					<tr>
						<th>
							<label>Children Ages:</label>
						</th>
						<td>
							<div class="regular-text">
								<?php
								if( $children_ages ){
									foreach( $children_ages as $ages => $val ){
										if( $val ){
											for($x=0;$x < count( $val );$x++){ 
												if( $x != count( $val ) - 1 ){ 
													if( $val[$x] ){
														echo $val[$x].',';
													} 
												} else {
													if( $val[$x] ){	
														echo $val[$x];
													}
												}
											} 
										}
									}
								} 	 
								?>
							</div> 
						</td> 
					</tr>  
					<tr>
						<th>
							<label>Total Amount Currency:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $total_amount_currency; ?></div> 
						</td>
						<th>
							<label>Total Amount:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $total_amount; ?></div> 
						</td>
					</tr>  
					<tr>
						<th colspan="3" >
							<label>Payment Information (<?php echo ucfirst($payment_type); ?>) </label>
						</th>
					</tr>
					<?php
					if( $payment_type == 'pesopay'){
						$payment_details = get_ticket_payment_pesopay( $id );
						$cppt_order_reference = get_value( $payment_details, 'cppt_order_reference' ); 
						$payment_pesopay_details = get_payment_pesopay_details( $cppt_order_reference );
						$cppt_amount = get_value( $payment_pesopay_details, 'cppt_amount' );
						$cppt_currency = get_value( $payment_pesopay_details, 'cppt_currency' );
						$cppt_holder_name = get_value( $payment_pesopay_details, 'cppt_holder_name' );
						$cppt_payment_method = get_value( $payment_pesopay_details, 'cppt_payment_method' );
						
						$cppt_currency_text = get_pesopay_currency( $cppt_currency );
						?>
						<tr>
							<th>
								<label>Order Reference:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_order_reference; ?></div> 
							</td> 
							<th>
								<label>Card Holder:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_holder_name; ?></div> 
							</td>   
						</tr>
						<tr> 
							<th>
								<label>Card Type:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_payment_method; ?></div> 
							</td> 
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $cppt_currency_text.' - '.$cppt_amount; ?></div> 
							</td> 
						</tr>
						<?php
						 
					} elseif( $payment_type == 'paypal'){
						$payment_details = get_ticket_payment_paypal( $id );
						$paypal_api_key = get_value( $payment_details, 'bpy_paypal_api_key' );
						$paypal_currency = get_value( $payment_details, 'bpy_total_amount_currency' );
						$paypal_amount = get_value( $payment_details, 'bpy_total_amount' );
						?>
						<tr>
							<th>
								<label>Paypal API:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $paypal_api_key; ?></div> 
							</td> 
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $paypal_currency.' - '.$paypal_amount; ?></div> 
							</td> 
						</tr>
						<?php
					} elseif( $payment_type == 'eghl'){
						$payment_details = get_ticket_payment_eghl( $id );
						$issuingbank = get_value( $payment_details, 'bghl_issuingbank' );
						$bankrefno = get_value( $payment_details, 'bghl_bankrefno' );
						$txnstatus = get_value( $payment_details, 'bghl_txnstatus' );
						$amount = get_value( $payment_details, 'bghl_amount' );
						$currencycode = get_value( $payment_details, 'bghl_currencycode' );  
						if( $txnstatus == 0 ){
							$txnstatus_text = 'Successful';
						} else {
							$txnstatus_text = 'Failed';
						}
						?>
						<tr>
							<th>
								<label>Bank:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $issuingbank; ?></div> 
							</td> 
							<th>
								<label>Bank Ref No:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $bankrefno; ?></div> 
							</td> 
						</tr>
						<tr>
							<th>
								<label>Amount:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $currencycode.' - '.$amount; ?></div> 
							</td> 
							<th>
								<label>Status:</label>
							</th>
							<td>
								<div class="regular-text"><?php echo $txnstatus_text; ?></div> 
							</td> 
						</tr>
						<?php
					}
					?>
				
					<tr>
						<th>
							<label>Modified:</label>
						</th>
						<td colspan="3">
							<div class="regular-text"><?php echo datetime($modified,'M j, Y \a\t H:ia'); ?></div> 
						</td> 
					</tr>  
					<tr>
						<th>
							<label>Created:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo datetime($created,'M j, Y \a\t H:ia'); ?></div> 
						</td>
						<th>
							<label>Published:</label>
						</th>
						<td>
							<div class="regular-text"><?php echo $published; ?></div> 
						</td>
					</tr>  
				</tbody>
			</table> 
			<div class="row">
				<div class="col-md-12 "> 
					<a href="<?php echo '?page='.$page.'&action=confirm_cancel&id='.$id;?>">
					<input class="col-md-5 btn btn-success" type="submit" name="client_and_payment_date_confirm" value="Cancel Booking" />  
					</a>
				</div>
			</div>
		</div>
		<?php
		
		
	}
}

function iwp_confirm_cancel_ticket_bookings_list_page(){
	global $wpdb; //This is used only if making any database queries  
	$id = $_GET['id'];
	$action = $_GET['action'];  
	$page = $_REQUEST['page']; 

	if( $id && $action == 'confirm_cancel' ){
		$ticket_api = new TicketAPI();
		$result_array = get_ticket_records( $id );
		 
		foreach( $result_array as $result ){
			$reference = get_value( $result,'tbk_reference' );  
	
			$cancellation_details = $ticket_api->get_ticket_booking_cancellation_details( $reference, 'CANCELLATION' );
			// printr( $cancellation_details );
			$errors_array = get_value( $cancellation_details,'errors' );  
			foreach( $errors_array as $errors_col ){
				$errors_code = get_value( $errors_col,'code' );  
				$errors_text = get_value( $errors_col,'text' );   
			}
			
			$booking_array = get_value( $cancellation_details, 'booking' );
			$booking_status = get_value( $booking_array, 'status' );  // CANCELLED 
		}
		
      	?>
		
		<div class="wrap"> 
			<div id="icon-users" class="icon32"><br/></div>
			<h2>Cancel Ticket and Excursions Booking</h2>
			<ul class="subsubsub">
				<li class="back"><a href="<?php echo '?page='.$page; ?>">Back</a></li>  
			</ul>
			<br /><br />
			<?php
			if( $booking_status == 'CANCELLED' ){
				if( $id ){ 
					$booking_updated = $wpdb->update( 
						'wp_cribsandtrips_ticket_booking', 
						array(  
							'tb_status' => 'cancelled',	// string 
						), 
						array( 'tb_id' => $id ) 
					); 	
				}
				?>
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-success" role="alert">
							<p>Booking is successfully cancelled. Cancellation Fee will be deducted to your refund amount if any.</p>
						</div> 
					</div>
				</div> 
				<?php
			} else {
				?>
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-danger" role="alert"> 
							<p>Booking can't cancelled. Please contact bedsonline for clarifacation.</p>
						</div> 
					</div>
				</div> 
				<?php
			}
			?>  			 
		</div>	
		<?php
	}
}

function get_ticket_payment_pesopay( $id ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $id ){ 
		$sql  = "SELECT *" 
			." FROM `wp_cribsandtrips_booking_pesopay` " 
			." WHERE"
			." `tb_id`='".$id."'"   
			;  	
		$result = $wpdb->get_row( $sql );
		 
		if( $result ){ 
			return $result;
		}
	}
} 

function get_ticket_payment_pesopay_details( $id ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $id ){ 
		$sql  = "SELECT *" 
			." FROM `wp_cribsandtrips_peso_pay_transactions` " 
			." WHERE"
			." `cppt_order_reference`='".$id."'"   
			;  	
		$result = $wpdb->get_row( $sql );
		 
		if( $result ){ 
			return $result;
		}
	}
} 

function get_ticket_pesopay_currency( $currency ){
   
	if( $currency == 344 ){
		$currency = 'HKD';
	} elseif( $currency == 840 ){
		$currency = 'USD';
	} elseif( $currency == 156 ){
		$currency = 'CNY';
	} elseif( $currency == 392 ){
		$currency = 'JPY';
	} elseif( $currency == 036 ){
		$currency = 'AUD';
	} elseif( $currency == 978 ){
		$currency = 'EUR';
	} elseif( $currency == 124 ){
		$currency = 'CAD';
	} elseif( $currency == 446 ){
		$currency = 'MOP';
	} elseif( $currency == 764 ){
		$currency = 'THB';
	} elseif( $currency == 458 ){
		$currency = 'MYR';
	} elseif( $currency == 410 ){
		$currency = 'KRW';
	} elseif( $currency == 682 ){
		$currency = 'SAR';
	} elseif( $currency == 784 ){
		$currency = 'AED';
	} elseif( $currency == 096 ){
		$currency = 'BND';
	} elseif( $currency == 356 ){
		$currency = 'INR';
	} elseif( $currency == 702 ){
		$currency = 'SGD';
	} elseif( $currency == 826 ){
		$currency = 'GBP';
	} elseif( $currency == 901 ){
		$currency = 'TWD';
	} elseif( $currency == 608 ){
		$currency = 'PHP';
	} elseif( $currency == 360 ){
		$currency = 'IDR';
	} elseif( $currency == 554 ){
		$currency = 'NZD';
	} elseif( $currency == 704 ){
		$currency = 'VND';
	}
	
	return $currency;
}

function get_ticket_payment_paypal( $id ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $id ){ 
		$sql  = "SELECT *" 
			." FROM `wp_cribsandtrips_booking_paypal` " 
			." WHERE"
			." `tb_id`='".$id."'"   
			;  	
		$result = $wpdb->get_row( $sql );
		 
		if( $result ){ 
			return $result;
		}
	}
}  

function get_ticket_payment_eghl( $id ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $id ){ 
		$sql  = "SELECT *" 
			." FROM `wp_cribsandtrips_booking_eghl` " 
			." WHERE"
			." `tb_id`='".$id."'"   
			;  	
		$result = $wpdb->get_row( $sql );
		 
		if( $result ){ 
			return $result;
		}
	}
} 

function get_ticket_country_name( $cub_country_code ){
	global $wpdb; //This is used only if making any database queries  
	
	if( $cub_country_code ){ 
		$sql_country_code  = "SELECT *" 
			." FROM `wp_cribsandtrips_country_ids` " 
			." WHERE"
			." `CountryCode`='".$cub_country_code."'"  
			." AND `LanguageCode`='ENG'"  
			;  	
		$result_country_code = $wpdb->get_row( $sql_country_code );
		$cub_country = get_value( $result_country_code, 'Name' );
		if( $cub_country ){ 
			return $cub_country;
		}
	}
} 

function iwp_ticket_months_year_dropdown(){ 
	global $wpdb, $wp_locale;  
	$selected_year = isset($_GET['year'])?$_GET['year']:0; 
	$selected_month = isset($_GET['month'])?$_GET['month']:0; 
		
	/* $months = $wpdb->get_results( "
		SELECT DISTINCT YEAR( book_created ) AS year, MONTH( book_created ) AS month
		FROM wp_cribsandtrips_booking 
		ORDER BY book_created DESC GROUP BY book_created" ); */
		
	$months = $wpdb->get_results( "SELECT DISTINCT MONTH( tb_created ) AS month FROM wp_cribsandtrips_ticket_booking GROUP BY tb_created ORDER BY tb_created ASC;" );
	
	$year = $wpdb->get_results( "SELECT DISTINCT YEAR( tb_created ) AS year FROM wp_cribsandtrips_ticket_booking GROUP BY tb_created ORDER BY tb_created DESC;" ); 
	
	$month_count = count( $months );
 
	if ( !$month_count ){ 
		return;
	}
	$m = isset( $_GET['m'] ) ? (int) $_GET['m'] : 0;
	?>
	<label for="filter-by-date" class="screen-reader-text"><?php _e( 'Filter by date' ); ?></label>
	<select name="month" id="filter-by-month">
		<option<?php selected( $m, 0 ); ?> value="0"><?php _e( 'All Months' ); ?></option>
		<?php
        foreach ( $months as $arc_row ) { 
            $month = zeroise( $arc_row->month, 2 ); 
			$month_name = $wp_locale->get_month( $month ); 
			if( $selected_month == $month ){
				echo '<option value='.$month.' selected>'.$month_name.'</option>'; 
			} else {
				echo '<option value='.$month.'>'.$month_name.'</option>'; 
			}
			
        }
		?>
	</select>
	<select name="year" id="filter-by-year">
		<option<?php selected( $m, 0 ); ?> value="0"><?php _e( 'All Year' ); ?></option>
		<?php
		$year_counter = '';
        foreach( $year as $ary_row ){ 
			$year = esc_attr( $ary_row->year );
			if( $year_counter != $year ){
				if( $selected_year == $year ){	
					echo '<option value='.$year.' selected>'.$year.'</option>';
				} else {
					echo '<option value='.$year.'>'.$year.'</option>';
				}
				$year_counter = $year;
			} 
        }
		?>
	</select>    
	<?php 
}

function iwp_ticket_date_selection(){
	global $wpdb, $wp_locale; 
	$selected_start_date = $_GET['start_date']; 
	$selected_end_date = $_GET['end_date'];  
	?> 
	<input type="date" id="start_date" name="start_date" class="example-datepicker" value="<?php echo $selected_start_date;?>" Placeholder="Select Start Date" style="width: 130px;"/>  
	<input type="date" id="end_date" name="end_date" class="example-datepicker" value="<?php echo $selected_end_date;?>" Placeholder="Select End Date" style="width: 130px;" />   
	<?php 
}
 
function iwp_ticket_bookings_render_income_graph(){
  
	global $wpdb; //This is used only if making any database queries  
	
	$page = $_REQUEST['page']; 
	  
	$year_today = date('Y'); 
	$status_type = $_GET['status_type'];  
	$month = $_GET['month'];  
	$filter_months_year = $_GET['filter-months-year-graph-submit'];  
	$filter_date = $_GET['filter-date-graph-submit'];  
	$get_year = $_GET['year'];
	$year = isset( $_GET['year'] )?$_GET['year']:$year_today;
		
	$start_date = $_GET['start_date'];  
	$end_date = $_GET['end_date'];  
	$s_month = $_GET['s_month'];  
	 
	$month_data = array();
	$data = array();
	$title_text = '';
	
	
	if( $month && $year && $filter_months_year == 'Filter'){
		 
		$sql_month = "SELECT DAY(LAST_DAY(`tb_created`)) as day_count"
			." FROM `wp_cribsandtrips_ticket_booking`"  
			." WHERE"  
			." `tb_published`='1' "    
			." AND MONTH( `tb_created`) = '".$month."' " 
			." AND YEAR( `tb_created` ) = '".$year."' "; 
			;
			
		$month_count_result = $wpdb->get_row( $sql_month ); 
		$day_count = get_value( $month_count_result, 'day_count' );
		 
		for($i=1;$i<=$day_count;$i++){ 
			$x_display = $month.'/'.$i.'/'.$year; 
			$month_data[] = "'".$i."'";
			$x_display = date( "Y-m-d", strtotime( $x_display ) ); 
		  
			
			$sql_day = "SELECT sum(`b`.`tbk_total_amount`) as total_amount"
			." FROM `wp_cribsandtrips_ticket_booking` AS `a`"  
			." LEFT JOIN `wp_cribsandtrips_ticket_booking_details` AS `b`"
			." ON `a`.`tb_id`=`b`.`tb_id`"  
			." WHERE"  
			." `a`.`tb_published`='1'"     
			." AND DATE( `a`.`tb_created` )='".$x_display."'"    
			; 	
			$day_count_result = $wpdb->get_results( $sql_day ); 
			
			if( $day_count_result ){
				foreach( $day_count_result as $value ){ 
					$month_number = $value->month;
					$total_amount = $value->total_amount; 
					$data[] = round($total_amount, 2);  
				} 
			} 
		}
		if( $month == '1' ){
			$month_name = 'Jan';
		} elseif( $month == '2' ){
			$month_name = 'Feb';
		} elseif( $month == '3' ){ 
			$month_name = 'Mar';
		} elseif( $month == '4' ){
			$month_name = 'Apr';
		} elseif( $month == '5' ){
			$month_name = 'May';
		} elseif( $month == '6' ){
			$month_name = 'Jun';
		} elseif( $month == '7' ){
			$month_name = 'Jul';
		} elseif( $month == '8' ){
			$month_name = 'Aug';
		} elseif( $month == '9' ){
			$month_name = 'Sep';
		} elseif( $month == '10' ){
			$month_name = 'Oct';
		} elseif( $month == '11' ){
			$month_name = 'Nov';
		} elseif( $month == '12' ){
			$month_name = 'Dec';
		}
		
		$title_text = 'Monthly Booking Total Income for '.$month_name.' '.$year;
		
	} elseif( $start_date && $end_date && $filter_date == 'Filter'){ 
	
		$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
		$end_date = date( "Y-m-d", strtotime( $end_date ) );
		
		$interval = new DateInterval('P1D'); 
		$realEnd = new DateTime($end_date);
		$realEnd->add($interval);

		$period = new DatePeriod(
			 new DateTime($start_date),
			 $interval,
			 $realEnd
		);
		if( $period ){ 
			foreach($period as $date) { 
				$x_display = $date->format('Y-m-d'); 
				$x_display_day = $date->format('d'); 
				$month_data[] = "'".$x_display_day."'";
				$x_display = date( "Y-m-d", strtotime( $x_display ) ); 
			  
				
				$sql_day = "SELECT sum(`b`.`tbk_total_amount`) as total_amount"
				." FROM `wp_cribsandtrips_ticket_booking` AS `a`"  
				." LEFT JOIN `wp_cribsandtrips_ticket_booking_details` AS `b`"
				." ON `a`.`tb_id`=`b`.`tb_id`"  
				." WHERE"  
				." `a`.`tb_published`='1'"     
				." AND DATE( `a`.`tb_created` )='".$x_display."'"    
				; 	
				$day_count_result = $wpdb->get_results( $sql_day ); 
				
				if( $day_count_result ){
					foreach( $day_count_result as $value ){ 
						$month_number = $value->month;
						$total_amount = $value->total_amount; 
						$data[] = round($total_amount, 2);  
					} 
				} 
			}  
		}  
		$title_text = 'Monthly Booking Total Income From: '.$start_date.' To: '.$end_date;
    } else {
		$sql = "SELECT MONTH(`a`.`tb_created`) month, sum(`b`.`tbk_total_amount`) as total_amount"
			." FROM `wp_cribsandtrips_ticket_booking` AS `a`"  
			." LEFT JOIN `wp_cribsandtrips_ticket_booking_details` AS `b`"
			." ON `a`.`tb_id`=`b`.`tb_id`"   
			." WHERE"  
			." `a`.`tb_published`='1'"    
			; 	
		 
		if( $month ){
			$sql .= " AND MONTH( `tb_created`) = '".$month."' "; 
		}
		
		if( $year ){
			$sql .= " AND YEAR( `tb_created` ) = '".$year."' "; 
		}
		
		if( $start_date && $end_date ){ 
			$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
			$end_date = date( "Y-m-d", strtotime( $end_date ) );  
			  
			$sql .= " AND ( DATE( `tb_created` ) BETWEEN '".$start_date."' AND '".$end_date."')";    
		}
		 
		$sql .=" GROUP BY  MONTH( `tb_created` )";
	 
		$result = $wpdb->get_results( $sql );
		 
		
		if( $result ){
			foreach( $result as $value ){ 
				$month_number = $value->month;
				$total_amount = $value->total_amount; 
				if( $month_number == '1' ){
					$month_name = 'Jan';
				} elseif( $month_number == '2' ){
					$month_name = 'Feb';
				} elseif( $month_number == '3' ){ 
					$month_name = 'Mar';
				} elseif( $month_number == '4' ){
					$month_name = 'Apr';
				} elseif( $month_number == '5' ){
					$month_name = 'May';
				} elseif( $month_number == '6' ){
					$month_name = 'Jun';
				} elseif( $month_number == '7' ){
					$month_name = 'Jul';
				} elseif( $month_number == '8' ){
					$month_name = 'Aug';
				} elseif( $month_number == '9' ){
					$month_name = 'Sep';
				} elseif( $month_number == '10' ){
					$month_name = 'Oct';
				} elseif( $month_number == '11' ){
					$month_name = 'Nov';
				} elseif( $month_number == '12' ){
					$month_name = 'Dec';
				}
				 
				$month_data[] = "'".$month_name."'"; 
				$data[] = round($total_amount, 2);  
			}
		}  
		$month_data_keys = array_keys($month_data);   
		$month_data_count = count( $month_data );

		$data_keys = array_keys($data);   
		$data_count = count( $data ); 
	}
	
	if( empty( $title_text )){
		if( $year ){
			$title_text = 'Monthly Booking Total Income for '.$year;
		} else {
			$title_text = 'Monthly Booking Total Income';
		}
		
	} 
	 
    ?> 
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script> 
	
	 
	<script type="text/javascript">
	jQuery(function () {
		jQuery('div#income_graph_container').highcharts({ 
			title: {
				text: '<?php echo $title_text; ?>',
				x: -20 //center
			}, 
			xAxis: {
				categories: [<?php echo join($month_data, ','); ?>]
			},
			yAxis: { 
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			}, 
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			}, 
			 tooltip: { 
                enabled: true,
                formatter: function() { 
                    return 'Amount Php: <b>'+ this.y.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") +'</b>'; 
                }
            },
			series: [{
				name: 'Booking Amount',
				data: [<?php echo join($data, ','); ?>],
				 
			}] 
		});
	});
	</script>
    <div class="wrap"> 
		<div id="icon-users" class="icon32"><br/></div>
        <h2>Tickets and Excursion Bookings Income Graph</h2>
        <ul class="subsubsub">  
			<li class="all"><a href="?page=iwp_ticket_bookings_list">Ticket and Excursions Booking List</a></li>
			<li class="all">&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>">Reset</a></li> 
		</ul> 
		<br /><br />
		<table>
			<tr>
				<td>
					<form id="income_month_year" method="get"> 
						<input type="hidden" value="<?php echo $page; ?>" name="page">
						<?php
						iwp_ticket_months_year_dropdown(); 
						?>
						<input type="submit" name="filter-months-year-graph-submit" class="button" value="Filter"/>
					</form>
				</td>
				<td>
					<form id="income_date_range" method="get"> 
						<input type="hidden" value="<?php echo $page; ?>" name="page">
						<?php 
						iwp_ticket_date_selection();
						?> 
						<input type="submit" name="filter-date-graph-submit" class="button" value="Filter"/>
					</form>  
				</td>
			</tr>
		</table> 
		<div id="income_graph_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
    <?php
}

function iwp_ticket_bookings_render_statistics_graph(){
  
	global $wpdb; //This is used only if making any database queries  
	$year_today = date('Y'); 
	$page = $_REQUEST['page'];  
	$status_type = $_GET['status_type'];  
	$filter_months_year = $_GET['filter-months-year-graph-submit'];  
	$filter_date = $_GET['filter-date-graph-submit'];  
	$month = $_GET['month'];   
	$get_year = $_GET['year'];
	$year = isset( $_GET['year'] )?$_GET['year']:$year_today;	
		
	$start_date = $_GET['start_date'];  
	$end_date = $_GET['end_date'];  
	
	$month_data = array();
	$data = array();
	$title_text = '';
	
	if( $month && $year && $filter_months_year == 'Filter'){ 
		$sql_month = "SELECT DAY(LAST_DAY(tb_created)) as day_count"
			." FROM `wp_cribsandtrips_ticket_booking`"  
			." WHERE"  
			." `book_published`='1' "    
			." AND MONTH( `tb_created`) = '".$month."' " 
			." AND YEAR( `tb_created` ) = '".$year."' "; 
			;
			
		$month_count_result = $wpdb->get_row( $sql_month ); 
		$day_count = get_value( $month_count_result, 'day_count' );
		$day_count = (float)$day_count;
		for($i=1;$i<=$day_count;$i++){ 
			$x_display = $month.'/'.$i.'/'.$year;
			$month_data[] = "'".$i."'";
			$x_display = date( "Y-m-d", strtotime( $x_display ) );  
			$sql_day = "SELECT MONTH(`a`.`tb_created`) month, COUNT(*) count"
				." FROM `wp_cribsandtrips_ticket_booking` AS `a`"  
				." LEFT JOIN `wp_cribsandtrips_ticket_booking_details` AS `b`"
				." ON `a`.`tb_id`=`b`.`tb_id`"  
				." WHERE"  
				." `a`.`tb_published`='1'"   
				." AND DATE( `a`.`tb_created` )='".$x_display."'"    
				; 	
			$day_count_result = $wpdb->get_results( $sql_day ); 
			if( $day_count_result ){
				foreach( $day_count_result as $value ){  
					$data[] = $value->count; 
				} 
			}
			 
		}
		
		if( $month == '1' ){
			$month_name = 'Jan';
		} elseif( $month == '2' ){
			$month_name = 'Feb';
		} elseif( $month == '3' ){ 
			$month_name = 'Mar';
		} elseif( $month == '4' ){
			$month_name = 'Apr';
		} elseif( $month == '5' ){
			$month_name = 'May';
		} elseif( $month == '6' ){
			$month_name = 'Jun';
		} elseif( $month == '7' ){
			$month_name = 'Jul';
		} elseif( $month == '8' ){
			$month_name = 'Aug';
		} elseif( $month == '9' ){
			$month_name = 'Sep';
		} elseif( $month == '10' ){
			$month_name = 'Oct';
		} elseif( $month == '11' ){
			$month_name = 'Nov';
		} elseif( $month == '12' ){
			$month_name = 'Dec';
		}
		$title_text = 'Monthly Booking Average Count for '.$month_name.' '.$year;
	} elseif( $start_date && $end_date && $filter_date == 'Filter'){ 
	
		$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
		$end_date = date( "Y-m-d", strtotime( $end_date ) );
		
		$interval = new DateInterval('P1D'); 
		$realEnd = new DateTime($end_date);
		$realEnd->add($interval);

		$period = new DatePeriod(
			 new DateTime($start_date),
			 $interval,
			 $realEnd
		);
		if( $period ){ 
			foreach($period as $date) { 
				$x_display = $date->format('Y-m-d'); 
				$x_display_day = $date->format('d'); 
				$month_data[] = "'".$x_display_day."'";
				$x_display = date( "Y-m-d", strtotime( $x_display ) ); 
			  
				
				$sql_day = "SELECT MONTH(`a`.`tb_created`) month, COUNT(*) count"
					." FROM `wp_cribsandtrips_ticket_booking` AS `a`"  
					." LEFT JOIN `wp_cribsandtrips_ticket_booking_details` AS `b`"
					." ON `a`.`tb_id`=`b`.`tb_id`"  
					." WHERE"  
					." `a`.`tb_published`='1'"   
				." AND DATE( `a`.`tb_created` )='".$x_display."'"    
				; 	
				$day_count_result = $wpdb->get_results( $sql_day ); 
				 
				if( $day_count_result ){
					foreach( $day_count_result as $value ){  
						$data[]= $value->count; 
					} 
				} 
			}  
		}  
		$title_text = 'Monthly Booking Average Count From: '.$start_date.' To: '.$end_date;
	} else { 
		$sql = "SELECT MONTH(`a`.`tb_created`) month, COUNT(*) count"
			." FROM `wp_cribsandtrips_ticket_booking` AS `a`"  
			." LEFT JOIN `wp_cribsandtrips_ticket_booking_details` AS `b`"
			." ON `a`.`tb_id`=`b`.`tb_id`"  
			." WHERE"  
			." `a`.`tb_published`='1'"   
		;
		  
		if( $month ){
			$sql .= " AND MONTH( `tb_created`) = '".$month."' "; 
		}
		
		if( $year ){
			$sql .= " AND YEAR( `tb_created` ) = '".$year."' "; 
		}
		
		if( $start_date && $end_date ){ 
			$start_date = date( "Y-m-d", strtotime( $start_date ) ); 
			$end_date = date( "Y-m-d", strtotime( $end_date ) );  
			  
			$sql .= " AND ( DATE( `tb_created` ) BETWEEN '".$start_date."' AND '".$end_date."')";   
			 
		}
		$sql .=" GROUP BY  MONTH( `tb_created` )"; 
		$result = $wpdb->get_results( $sql );
		
		if( $result ){
			foreach( $result as $value ){ 
				$month_number = $value->month;
				$month_count = $value->count;  
				 
				if( $month_number == 1 ){
					$month_name = 'Jan';
				} elseif( $month_number == 2 ){
					$month_name = 'Feb';
				} elseif( $month_number == 3 ){ 
					$month_name = 'Mar';
				} elseif( $month_number == 4 ){
					$month_name = 'Apr';
				} elseif( $month_number == 5 ){
					$month_name = 'May';
				} elseif( $month_number == 6 ){
					$month_name = 'Jun';
				} elseif( $month_number == 7 ){
					$month_name = 'Jul';
				} elseif( $month_number == 8 ){
					$month_name = 'Aug';
				} elseif( $month_number == 9 ){
					$month_name = 'Sep';
				} elseif( $month_number == 10 ){
					$month_name = 'Oct';
				} elseif( $month_number == 11 ){
					$month_name = 'Nov';
				} elseif( $month_number == 12 ){
					$month_name = 'Dec';
				}
				 
				$month_data[] = "'".$month_name."'"; 
				$data[] = $month_count; 
			}
		}
	} 
	$month_data_keys = array_keys($month_data);   
	$month_data_count = count( $month_data );

	$data_keys = array_keys($data);   
	$data_count = count( $data ); 
	 
	if( empty( $title_text )){
		if( $year ){
			$title_text = 'Monthly Booking Average Count for '.$year;
		} else {
			$title_text = 'Monthly Booking Average Count';
		}
		
	} 
    ?> 
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script> 
	
	 
	<script type="text/javascript">
	jQuery(function () {
		jQuery('#statistics_graph_container').highcharts({ 
			title: {
				text: '<?php echo $title_text; ?>',
				x: -20 //center
			}, 
			xAxis: {
				categories: [<?php echo join( $month_data, ','); ?>]
			},
			yAxis: { 
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}]
			}, 
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				name: 'Booking',
				data: [<?php echo join($data, ','); ?>]
			} ]
		});
	});
	</script>
    <div class="wrap"> 
		<div id="icon-users" class="icon32"><br/></div>
        <h2>Ticket and Excursion Bookings Statistics Graph</h2>
		<ul class="subsubsub">  
			<li class="all"><a href="?page=iwp_ticket_bookings_list">Ticket and Excursions Booking List</a></li>
			<li class="all">&nbsp;|&nbsp;<a href="?page=<?php echo $page;?>">Reset</a></li> 
		</ul> 
		<br /><br />
		<table>
			<tr>
				<td>
					<form id="statistics_month_year" method="get"> 
						<input type="hidden" value="<?php echo $page; ?>" name="page">
						<?php
						iwp_ticket_months_year_dropdown(); 
						?>
						<input type="submit" name="filter-months-year-graph-submit" class="button" value="Filter"/>
					</form>
				</td>
				<td>
					<form id="statistics_date_range" method="get"> 
						<input type="hidden" value="<?php echo $page; ?>" name="page">
						<?php 
						iwp_ticket_date_selection();
						?> 
						<input type="submit" name="filter-date-graph-submit" class="button" value="Filter"/>
					</form> 
					
				</td>
			</tr>
		</table>
		<div id="statistics_graph_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
    <?php
} 